<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <div class="content">

        <form action="{{ action('TwitterController@search_tweet') }}" method="get">
            <label>Keyword:</label>
            <input type="text" name="keyword">
            <input type="submit" value="Search">
        </form>

        <hr>
        <table>
            <tr>
                <th>Keyword</th>
                <th>Qty</th>
            </tr>
            
        @foreach($keywords as $word => $qty)
            <tr>
                <td>{{ $word }}</td>
                <td>{{ $qty }}</td>
            </tr>
        @endforeach
        </table>

        </div>
    </body>
</html>