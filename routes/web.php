<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/instagram', 'InstagramController@index');
Route::get('/twittercount', 'TwitterController@count_twitter_words');
Route::get('/twitter', 'TwitterController@show_form');
Route::get('/twittersearch', 'TwitterController@search_tweet');