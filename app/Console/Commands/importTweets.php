<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Twitter;

class importTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tweets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import tweets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=julbord",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization:" /*insert authorization here*/,
            "cache-control: no-cache",
            "postman-token: 3dc7b821-b618-3745-aa0f-583099b54714"
          ),
        ));
        
        $response = json_decode(curl_exec($curl), true);

        foreach ($response['statuses'] as $tweet) {
            $this->info("Importing tweet with id: " . $tweet['id']);
            $tweets = Twitter::findOrNew($tweet['id']);
            $tweets->fill($tweet)->save();
        }
    }
}