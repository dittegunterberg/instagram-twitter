<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Twitter;

class TwitterController extends Controller
{
    public function count_twitter_words() {
        $twitter = Twitter::all();
        echo Twitter::count_words($twitter);
    }

    public function show_form() {
        return View('twitter');
    }

    public function search_tweet(Request $request) {
        $search = Twitter::show_tweets($request->keyword);
        $count = Twitter::count_sort($search);
        return View('twittersearch', ['keywords' => $count]);
    }
}