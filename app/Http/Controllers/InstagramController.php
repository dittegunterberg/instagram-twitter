<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instagram;


class InstagramController extends Controller
{
    public function index() {
           $images = Instagram::all();
           return View('/instagram', ['images' => $images]);
    }
}