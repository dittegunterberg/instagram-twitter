<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Twitter extends Model
{
    protected $table = 'tweets';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;
    
    protected $fillable = [
        "id",
        "created_at",
        "text"
    ];

    static public function count_words($twitter) {
        $tweets = $twitter->pluck('text');
        $tweets = json_encode($tweets);
        $tweets = substr($tweets, 2, -2);
        $tweets = strtolower($tweets);
        $array = explode(' ', $tweets);
        
        $stop_words = [
            "navidad",
            "lunch",
            "familj"
        ];

        $unique = array_unique($array);
        $words = array_diff($unique, $stop_words);

        foreach ($words as $word) {
            $count = substr_count($tweets, $word);
            echo "Ordet <b>" . $word . "</b> förekommer <b>" . $count . "</b> gånger.<br>";
        }
    }

    static public function show_tweets ($keyword) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q='.$keyword'",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization:" /*insert authorization here*/
                "cache-control: no-cache",
                "postman-token: 3dc7b821-b618-3745-aa0f-583099b54714"
            ),
            ));
            
            $response = json_decode(curl_exec($curl), true);
            $list = [];
            
            foreach ($response['statuses'] as $tweet) {
                $list[] = $tweet['text'];
            }
            
            curl_close($curl);
            return $list;
    }

    static function count_sort($tweets) {
        $list = [];

        foreach ($tweets as $tweet) {
            $explode = explode(" ", $tweet);
            $list = array_merge($list, $explode);
        }

        $count = array_count_values($list);
        arsort($count);
        return $count;
    }
}